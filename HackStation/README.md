# A Portable Hack Station

![Completed Project](Hackstation.jpg)

A portable hack station for working on the move. The top plate has space for a Raspberry Pi double breadboards and an Arduino Mega. This top layer is deep enough to close up the station when travelling and leave the hookup wires and module plugged in the breadboard. Ready for a quick setup when you get chance to hack a little more. The storage layer has space for two draws to carry breakout boards, modules, hookup wire, batteries and other essentials. 

![Hackstation Open](HackStationOpen.jpg)

 The outer slip cover closes up the hack-station for travel, holding the draws in place and stopping components from escaping. The current project on the top layer is safely enclosed to ensure nothing gets disconnected or damaged. If you want some extra security and peace of mind place a large rubber band around the station to ensure it does not get bumped apart in your bag.

![Hackstation Closed](HackStationClosed.jpg)

The SVG files 300200Sx.SVG are the 7 cut files laid out ready to go for a 300mm by 200mm laser cutter. All of these parts are needed to make a complete Hackstation. The design assumes the sheet material is 3mm thick.
